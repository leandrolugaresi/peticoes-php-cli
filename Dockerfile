FROM eminetto/docker-php7-cli

RUN apt-get update \
    && apt-get install -y software-properties-common git libmemcached-dev netcat mysql-client libpng12-dev libjpeg-dev libpq-dev \
    && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
    && docker-php-ext-install gd mbstring opcache pdo pdo_mysql zip \
    && git clone https://github.com/php-memcached-dev/php-memcached.git \
    && cd php-memcached \
    && git checkout php7 \
    && phpize \
    && ./configure --disable-memcached-sasl \
    && echo "init make for memcached" \
    && make \
    && make install \
    && echo "extension=memcached.so" >> /usr/local/etc/php/conf.d/memcached.ini \
    && docker-php-ext-install -j$(nproc) bcmath